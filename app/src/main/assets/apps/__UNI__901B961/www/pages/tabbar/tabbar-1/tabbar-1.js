"use weex:vue";

if (typeof Promise !== 'undefined' && !Promise.prototype.finally) {
  Promise.prototype.finally = function(callback) {
    const promise = this.constructor
    return this.then(
      value => promise.resolve(callback()).then(() => value),
      reason => promise.resolve(callback()).then(() => {
        throw reason
      })
    )
  }
};

if (typeof uni !== 'undefined' && uni && uni.requireGlobal) {
  const global = uni.requireGlobal()
  ArrayBuffer = global.ArrayBuffer
  Int8Array = global.Int8Array
  Uint8Array = global.Uint8Array
  Uint8ClampedArray = global.Uint8ClampedArray
  Int16Array = global.Int16Array
  Uint16Array = global.Uint16Array
  Int32Array = global.Int32Array
  Uint32Array = global.Uint32Array
  Float32Array = global.Float32Array
  Float64Array = global.Float64Array
  BigInt64Array = global.BigInt64Array
  BigUint64Array = global.BigUint64Array
};


(()=>{var g=Object.create;var b=Object.defineProperty;var f=Object.getOwnPropertyDescriptor;var x=Object.getOwnPropertyNames;var y=Object.getPrototypeOf,m=Object.prototype.hasOwnProperty;var h=(e,o)=>()=>(o||e((o={exports:{}}).exports,o),o.exports);var v=(e,o,l,a)=>{if(o&&typeof o=="object"||typeof o=="function")for(let n of x(o))!m.call(e,n)&&n!==l&&b(e,n,{get:()=>o[n],enumerable:!(a=f(o,n))||a.enumerable});return e};var w=(e,o,l)=>(l=e!=null?g(y(e)):{},v(o||!e||!e.__esModule?b(l,"default",{value:e,enumerable:!0}):l,e));var d=h((V,_)=>{_.exports=Vue});var t=w(d());function k(e){return weex.requireModule(e)}function r(e,o,...l){uni.__log__?uni.__log__(e,o,...l):console[e].apply(console,[...l,o])}var S={content:{"":{textAlign:"center",marginTop:"200rpx"}}},C=(e,o)=>{let l=e.__vccOpts||e;for(let[a,n]of o)l[a]=n;return l},N={data(){return{title:"Hello",msg:"\u6CA1\u6709\u901A\u77E5"}},onLoad(){uni.$on("reciveMsg",e=>{r("log","at pages/tabbar/tabbar-1/tabbar-1.nvue:25",1123),this.msg=JSON.stringify(e)})},methods:{selectFile(){},handleAdd(){k("zyx-plugin-module-one").selectFile({a:1,b:26},o=>{if(o.code==200){let l=o.result;r("log","at pages/tabbar/tabbar-1/tabbar-1.nvue:42",l),plus.io.resolveLocalFileSystemURL(l,a=>{r("log","at pages/tabbar/tabbar-1/tabbar-1.nvue:44",a),uni.showModal({content:JSON.stringify(a)})},a=>{r("log","at pages/tabbar/tabbar-1/tabbar-1.nvue:49",a),uni.showModal({content:JSON.stringify(a)})})}})}}};function A(e,o,l,a,n,i){let u=(0,t.resolveComponent)("button"),p=(0,t.resolveComponent)("zyx-plugin-input");return(0,t.openBlock)(),(0,t.createElementBlock)("scroll-view",{scrollY:!0,showScrollbar:!0,enableBackToTop:!0,bubble:"true",style:{flexDirection:"column"}},[(0,t.createElementVNode)("view",{class:"content"},[(0,t.createElementVNode)("u-text",null," \u9875\u9762 - 322222 "),(0,t.createElementVNode)("view",{class:"txt"},[(0,t.createElementVNode)("u-text",null,(0,t.toDisplayString)(n.msg),1)]),(0,t.createElementVNode)("u-text",null," \u8FD9\u91CC\u662F\u4E2A\u6309\u94AE "),(0,t.createVNode)(u,{type:"default",onClick:i.handleAdd},{default:(0,t.withCtx)(()=>[(0,t.createTextVNode)("add")]),_:1},8,["onClick"]),(0,t.createVNode)(p,{style:{height:"60rpx",width:"400rpx",border:"1rpx solid black"}}),(0,t.createVNode)(p,{style:{height:"60rpx",width:"400rpx",border:"1rpx solid black"}}),(0,t.createVNode)(u,{type:"default",onClick:i.selectFile},{default:(0,t.withCtx)(()=>[(0,t.createTextVNode)("select file")]),_:1},8,["onClick"])])])}var s=C(N,[["render",A],["styles",[S]]]);var c=plus.webview.currentWebview();if(c){let e=parseInt(c.id),o="pages/tabbar/tabbar-1/tabbar-1",l={};try{l=JSON.parse(c.__query__)}catch(n){}s.mpType="page";let a=Vue.createPageApp(s,{$store:getApp({allowDefault:!0}).$store,__pageId:e,__pagePath:o,__pageQuery:l});a.provide("__globalStyles",Vue.useCssStyles([...__uniConfig.styles,...s.styles||[]])),a.mount("#root")}})();
