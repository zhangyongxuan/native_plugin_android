package com.zhangyongxuan.zyxplugin.module;

import android.content.Context;
import android.widget.EditText;

import androidx.annotation.NonNull;

import io.dcloud.feature.uniapp.UniSDKInstance;
import io.dcloud.feature.uniapp.ui.action.AbsComponentData;
import io.dcloud.feature.uniapp.ui.component.AbsVContainer;
import io.dcloud.feature.uniapp.ui.component.UniComponent;


public class ComponentOne extends UniComponent<EditText> {
    public ComponentOne(UniSDKInstance instance, AbsVContainer parent, AbsComponentData componentData) {
        super(instance, parent, componentData);
    }

    @Override
    protected EditText initComponentHostView(@NonNull Context context) {
        return new EditText(context);
    }

}
