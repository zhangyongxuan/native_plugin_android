package com.zhangyongxuan.zyxplugin.module;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.Nullable;

import com.alibaba.fastjson.JSONObject;
import com.dyne.homeca.demo.VideoActivity;


import java.io.File;

import io.dcloud.feature.uniapp.annotation.UniJSMethod;
import io.dcloud.feature.uniapp.bridge.UniJSCallback;
import io.dcloud.feature.uniapp.common.UniModule;

public class ModuleOne extends UniModule {
    private UniJSCallback fileSelectCallback;
    private int FILE_SELECT = 10000;  // 选择文件
    private int IMG_SELECT = 10001;  // 选择文件
    private  int REQUEST_CODE = 10002;
    final String DOC = "application/msword";
    final String XLS = "application/vnd.ms-excel";
    final String PPT = "application/vnd.ms-powerpoint";
    final String DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    final String XLSX = "application/x-excel";
    final String PPTX = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
    final String PDF = "application/pdf";
    final String MP4 = "video/mp4";
    final String M3U8 = "application/x-mpegURL";
    private Context ctx;

    @UniJSMethod(uiThread = true)
    public void selectFile(JSONObject json, UniJSCallback callback) {
        fileSelectCallback = callback;
            try {
                Intent intent = new Intent(mUniSDKInstance.getContext(), VideoActivity.class );
                ((Activity)mUniSDKInstance.getContext()).startActivityForResult(intent,REQUEST_CODE);
                JSONObject data = new JSONObject();
                data.put("code", 200);
                data.put("msg","打开播放器成功");
            }catch (Exception e){
                e.printStackTrace();
            }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri uri = data.getData();
        if (requestCode == FILE_SELECT) {
            String  mFilePath = Uri.decode(data.getDataString());
            String m = mFilePath.substring(7, mFilePath.length());
            JSONObject jso = new JSONObject();
            jso.put("code", 200);
            jso.put("result", uri.getPath());
            fileSelectCallback.invoke(jso);

        }
        if (requestCode == REQUEST_CODE) {
            String  mFilePath = Uri.decode(data.getDataString());
            String m = mFilePath.substring(7, mFilePath.length());
            JSONObject jso = new JSONObject();
            jso.put("code", 200);
            jso.put("result", uri.getPath());
            fileSelectCallback.invoke(jso);

        }

    }
}
