package com.dyne.homeca.demo;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.dyne.homeca.bean.CameraInfo;
import com.raycommtech.ipcam.Code;
import com.zhangyongxuan.zyxplugin.module.R;

import java.lang.ref.WeakReference;


public class VideoActivity extends Activity implements SurfaceHolder.Callback, TextureView.SurfaceTextureListener, View.OnLongClickListener
{
    private static final String TAG = "VideoActivity";



    private ImageButton backButton;
    private RelativeLayout mVideoFrame;

    private boolean mIsTalking = false;
    private boolean mIsRecording = false;
    private boolean mIsListening = false;
    private boolean mIsVodPlaying = false;
    private int mVodPauseFlag = 0;
    private int mChannel = 0;

    private int mVidWidth = 0;
    private int mVidHeight = 0;
    private MediaFetchWrap mSingleMediaFetchWrap = null;

    private Parameters mParameters[] = new Parameters[4];

    private class Parameters
    {
        String mLinkType;
        int mDecodeType;
        String mDDNS;
        String mUID;
        int mChannel;
        String mUser;
        String mPassword;

        View mWindow;
        MediaFetchWrap mMediaFetchWrap;
    }

    private class WorkHandler extends Handler
    {
        WeakReference<MediaFetchWrap> mMediaFetchWrap;

        public WorkHandler(MediaFetchWrap mediaFetchWrap)
        {
            mMediaFetchWrap = new WeakReference<MediaFetchWrap>(mediaFetchWrap);
        }

        @Override
        public void handleMessage(Message msg)
        {
            final MediaFetchWrap mediaFetchWrap = mMediaFetchWrap.get();
            if (mediaFetchWrap != null)
            {
                switch (msg.what)
                {
                    case Code.MSG_START_PLAY:
//                        mediaFetchWrap.snap();
                        break;
                    case Code.MSG_OPENCAMERA_SUCCESS:
                        mediaFetchWrap.signalOpenCamera(true);
                        mediaFetchWrap.startMonitor(0);
                        break;
                    case Code.MSG_OPENCAMERA_FAILURE:
                        mediaFetchWrap.signalOpenCamera(false);
                        mediaFetchWrap.release();
                        break;
                    case Code.MSG_RECORD_START_PLAY_SUCCESS:
                        break;
                    case Code.MSG_RECORD_START_PLAY_FAILED:
                        break;
                    case Code.MSG_OPENCAMERA_VERIFY_FAILED:
                        break;
                    case Code.MSG_INFO:
                        Toast.makeText(VideoActivity.this, msg.obj.toString(), Toast.LENGTH_SHORT).show();
                        break;
                    case Code.MSG_ERROR:
//                        mediaFetchWrap.release();
                        Toast.makeText(VideoActivity.this, msg.obj.toString(), Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }
    }

    private MediaFetchWrap openCamera(String linktype, int decodetype, View view, String ddns, String uid, int channel, String user, String password)
    {
        Log.i(TAG, "openCamera " + linktype + " " + decodetype + " " + ddns + " " + uid + " " + channel + " " + user + " " + password);

        CameraInfo cameraInfo;
        //创建 CameraInfo 对象
        cameraInfo = new CameraInfo();
        //设置设备序列号
        cameraInfo.setCamerain(uid);
        //设置设备服务器
        cameraInfo.setCameraserverurl(ddns);
        //备用字段,当前可以随意输入
        cameraInfo.setCamerasn("aa");
        cameraInfo.setLinkedtype(linktype);
        //创建 MediaFetchWrap 对象
        MediaFetchWrap mediaFetchWrap = new MediaFetchWrap(this, cameraInfo, view, user, password, decodetype, channel);
        mediaFetchWrap.setWorkHandler(new WorkHandler(mediaFetchWrap));
        mediaFetchWrap.connectCamera();

        return mediaFetchWrap;
    }

    public void changeViewSize(View view, int left, int top, int width, int height)
    {
        // 更改视图尺寸
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(width, height);
        params.leftMargin = left;
        params.topMargin = top;
        view.setLayoutParams(params);
    }

    public void initVideoFrame()
    {
       try {
           mParameters[0].mWindow = new SurfaceView(this);
           mParameters[0].mWindow.setKeepScreenOn(true);
           SurfaceView window1 = (SurfaceView)mParameters[0].mWindow;
           window1.getHolder().addCallback(this);
           window1.setOnLongClickListener(this);
           mVideoFrame.addView(mParameters[0].mWindow);
           changeViewSize(mParameters[0].mWindow, 0, 0, mVidWidth, mVidHeight);
       }catch (Exception e){
           e.printStackTrace();
       }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        Log.i(TAG, "surfaceCreated");

        SurfaceView window1 = (SurfaceView)mParameters[0].mWindow;
        if(window1.getHolder() == holder && mParameters[0].mLinkType != null)
        {
            mParameters[0].mMediaFetchWrap = openCamera(mParameters[0].mLinkType, mParameters[0].mDecodeType, mParameters[0].mWindow,
                    mParameters[0].mDDNS, mParameters[0].mUID, mParameters[0].mChannel, mParameters[0].mUser, mParameters[0].mPassword);
            window1.performLongClick();
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
        Log.i(TAG, "surfaceChanged");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {
        Log.i(TAG, "surfaceDestroyed");

        SurfaceView window1 = (SurfaceView)mParameters[0].mWindow;

        if(window1 != null && window1.getHolder() == holder)
        {
            if(mParameters[0].mMediaFetchWrap != null)
            {
                mParameters[0].mMediaFetchWrap.release();
            }
            mParameters[0].mWindow = null;
        }

    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height)
    {
        Log.i(TAG, "onSurfaceTextureAvailable");

    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height)
    {
        Log.i(TAG, "onSurfaceTextureSizeChanged");
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface)
    {
        Log.i(TAG, "onSurfaceTextureDestroyed");



        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface)
    {
//        Log.i(TAG, "onSurfaceTextureUpdated");
    }

    @Override
    public boolean onLongClick(View v)
    {


        return true;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video);

//        backButton = findViewById(R.id.imageButton2);
//        backButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.d(TAG, "onClick: back");
//                ProgressDialog loading =  new ProgressDialog(that);
//                loading.setMessage("加载中。。。");
//                loading.setCancelable(false);
//                loading.show();
//
//            }
//        });

        FileUtils.init(this);

        final DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
        int res_width = metrics.widthPixels;
        int res_height = metrics.heightPixels;

        Resources resources = getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        int nav_height = resources.getDimensionPixelSize(resourceId);

//        LinearLayout buttonPanel = findViewById(R.id.buttonPanel);
//        buttonPanel.measure(0, 0);
//        int but_width = buttonPanel.getMeasuredWidth();
//        int but_height = buttonPanel.getMeasuredHeight();

        mVideoFrame = findViewById(R.id.videoframe);
        mVidWidth = res_width  - nav_height;
        mVidHeight = res_height;
        mVideoFrame.getLayoutParams().width = mVidWidth;
        mVideoFrame.getLayoutParams().height = mVidHeight;

        for(int i = 0;i < mParameters.length;i++)
        {
            mParameters[i] = new Parameters();
        }

        mParameters[0].mLinkType = "TCP";
        mParameters[0].mDecodeType = 1;
        mParameters[0].mDDNS = "wx11.sdvideo.cn";
        mParameters[0].mUID = "3HK6E110116EMB2";
        mParameters[0].mChannel = 0;
        mParameters[0].mUser = "admin";
        mParameters[0].mPassword = "12345";

//        mParameters[1].mLinkType = "P2P";
//        mParameters[1].mDecodeType = 0;
//        mParameters[1].mDDNS = "http://jordan.sdvideo.cn";
//        mParameters[1].mUID = "3HKA012065TKFPN";
//        mParameters[1].mChannel = 0;
//        mParameters[1].mUser = "admin";
//        mParameters[1].mPassword = "123456";

//        mParameters[2].mLinkType = "P2P";
//        mParameters[2].mDecodeType = 1;
//        mParameters[2].mDDNS = "http://wx19.sdvideo.cn";
//        mParameters[2].mUID = "3HKE022455K3AYJ";
//        mParameters[2].mChannel = 0;
//        mParameters[2].mUser = "admin";
//        mParameters[2].mPassword = "12345";

//        mParameters[3].mLinkType = "TCP";
//        mParameters[3].mDecodeType = 1;
//        mParameters[3].mDDNS = "ddns.cmqly.com:8017";
//        mParameters[3].mUID = "Q01030A210100E2QEB4U";
//        mParameters[3].mChannel = 0;
//        mParameters[3].mUser = "admin";
//        mParameters[3].mPassword = "c4HTxmJ2dt";

        initVideoFrame();

        final Context that = this;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {

    }

    @Override
    protected void onPause()
    {
        super.onPause();


    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();


    }
}