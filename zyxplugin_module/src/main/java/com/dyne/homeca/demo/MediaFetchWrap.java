package com.dyne.homeca.demo;

import static android.os.AsyncTask.THREAD_POOL_EXECUTOR;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;

import com.dyne.homeca.bean.CameraInfo;
import com.dyne.homeca.newtask.GenericTask;
import com.dyne.homeca.newtask.TaskManager;
import com.dyne.homeca.newtask.TaskResult;
import com.raycommtech.ipcam.Code;
import com.raycommtech.ipcam.MediaFetch;
import com.raycommtech.ipcam.MediaFetchFactory;
import com.raycommtech.ipcam.VideoInfo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MediaFetchWrap
{
    public static final String TAG = "MediaFetchWrap";
    MediaFetch mMediaFetch;
    private View mView;
    private CameraInfo mCameraInfo;
    private int mDecodeType;
    private int mChannelID;
    private String mUser;
    private String mPassword;

    private Handler mWorkHandler;
    private Context context;
    private boolean cameraOpen;

    private ProgressDialog loading;

    public enum Mode
    {
        NONE, MONITOR, PLAY
    }

    Mode mMode = Mode.NONE;

    public MediaFetchWrap(Context context, CameraInfo cameraInfo, View view, String user, String password, int decodetype, int channel)
    {
        this.context = context;
        mCameraInfo = cameraInfo;

        mView = view;
        mUser = user;
        mPassword = password;
        mDecodeType = decodetype;
        mChannelID = channel;
        loading = new ProgressDialog(context);
        loading.setMessage("加载中。。。");
        loading.setCancelable(false);
    }

    public void setWorkHandler(Handler handler)
    {
        this.mWorkHandler = handler;
    }

    public CameraInfo getmCameraInfo()
    {
        return mCameraInfo;
    }

    public static class CameraTask extends GenericTask
    {
        private MediaFetchWrap mMediaFetchWrap;

        public CameraTask(MediaFetchWrap mediaFetchWrap)
        {
            super(mediaFetchWrap.context);
            mMediaFetchWrap = mediaFetchWrap;
        }

        @Override
        protected void onCancelled()
        {
            super.onCancelled();
            mMediaFetchWrap.closeMediaFetch(false);
        }

        @Override
        protected void onPostExecute(Bundle result)
        {
            super.onPostExecute(result);
        }

        @Override
        protected Bundle _doInBackground(Object... params)
        {
            TaskResult lTaskResult = mMediaFetchWrap.checkConnect();
            taskResult.putSerializable(GenericTask.RESULT, lTaskResult);
            return taskResult;
        }
    }

    private TaskManager taskManager = new TaskManager();

    ReentrantReadWriteLock lockConnection = new ReentrantReadWriteLock();
    private Lock lockWaitConnectingResult = new ReentrantLock();
    private Condition conditionWaitConnectingResult = lockWaitConnectingResult.newCondition();

    TaskResult checkConnect()
    {
        if (lockConnection.writeLock().tryLock() == false)
        {
            return TaskResult.CONNECT_BUSY;
        }

        lockWaitConnectingResult.lock();
        try
        {
            if (mMediaFetch == null || cameraOpen == false)
            {
                VideoInfo vi = new VideoInfo();
                vi.setDdnsServer(mCameraInfo.getCameraserverurl());
                vi.setDdnsname(mCameraInfo.getPureCamerain());

//                CameraFun.getStatus(mCameraInfo.getCameraserverurl(), mCameraInfo.getPureCamerain());
//                if(vi == null)
//                {
//                    return TaskResult.FAILED;
//                }
//                mCameraInfo.setVideoInfo(vi);

                if (mCameraInfo.getLinkedtype().equals("P2P")) //P2P转发方式
                {
                    vi.SetDistributeType(false);
                }
                else if (mCameraInfo.getLinkedtype().equals("TCP"))//转发方式
                {
                    vi.SetDistributeType(true);
                }

                vi.setLinkTypeId(2);
                vi.setChannelId(mChannelID);
                vi.setUsername(mUser);
                vi.setPassword(mPassword);
                vi.setDecoderType(mDecodeType);

                mMediaFetch = MediaFetchFactory.makeMeidaFetch(mWorkHandler, mView, vi);
                mMediaFetch.setEncryptCallback(new MediaFetch.encryptCallback()
                {
                    @Override
                    public void onVideoEncrypted()
                    {
                        super.onVideoEncrypted();
                        Log.i(TAG, "onVideoEncrypted");
                    }
                });
            }
            if (mMediaFetch == null)
                return TaskResult.OPENCAMERAFAILD;
            else if (cameraOpen == false)
            {
                mMediaFetch.opencamera();
                conditionWaitConnectingResult.await(10, TimeUnit.SECONDS);
            }

            if (cameraOpen)
            {
                return TaskResult.OK;
            }
            else
            {
                return TaskResult.OPENCAMERAOUTOFTIME;
            }

        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
            return TaskResult.TASKINTERRUPTED;
        }
        finally
        {
            lockWaitConnectingResult.unlock();
            lockConnection.writeLock().unlock();

        }
    }

    public void signalOpenCamera(boolean success)
    {
        lockWaitConnectingResult.lock();
        try
        {
            cameraOpen = success;
            conditionWaitConnectingResult.signalAll();
        }
        finally
        {
            lockWaitConnectingResult.unlock();
        }
    }

    public void closeMediaFetch(boolean mandatory)
    {
        if (mandatory)
            lockConnection.writeLock().lock();
        else
        {
            if (lockConnection.writeLock().tryLock() == false)
                return;
        }

        try
        {
            if (mMediaFetch != null)
            {
                if (mMode == Mode.PLAY)
                    mMediaFetch.VODStopPlayRecord();
                if (mMode == Mode.MONITOR)
                    mMediaFetch.StopRealPlay();
                mMediaFetch.closecamera();
                mMediaFetch = null;
            }
            cameraOpen = false;
            mMode = Mode.NONE;
            mIsAudio = false;
            mIsTalk = false;
            mIsRecording = false;
        }
        finally
        {
            lockConnection.writeLock().unlock();
        }
    }

    public void release()
    {
        taskManager.cancelAll();

        loading.show();
        if (mDelayStop != null)
            mWorkHandler.removeCallbacks(mDelayStop);

        closeMediaFetch(true);
    }

    public void connectCamera()
    {
        CameraTask task = new CameraTask(this);
        task.executeOnExecutor(THREAD_POOL_EXECUTOR);
    }

    private boolean mCanControl = true;

    public boolean isPlaying()
    {
        return mMediaFetch != null && mMode == Mode.PLAY;
    }

    public boolean isMonitoring()
    {
        return mMediaFetch != null && mMode == Mode.MONITOR;
    }

    public boolean ismCanControl()
    {
        return mCanControl;
    }

    public void setmCanControl(boolean mCanControl)
    {
        this.mCanControl = mCanControl;
    }

    //转到预置位
    public void preGo(int i)
    {
        if (isMonitoring() && ismCanControl())
        {
            mMediaFetch.preGo(i);
        }
    }

    //转到云台
    public void ptzGo(int i)
    {
        if (isMonitoring() && ismCanControl())
        {
            mMediaFetch.ptzGo(i);
        }
    }

    //云台向上
    public void ptzGoUp()
    {
        ptzGo(Code.UP);
    }

    //云台向下
    public void ptzGoDown()
    {
        ptzGo(Code.DOWN);
    }

    //云台向左
    public void ptzGoLeft()
    {
        ptzGo(Code.LEFT);
    }

    //云台向右
    public void ptzGoRight()
    {
        ptzGo(Code.RIGHT);
    }

    //云台向上然后停止
    public void ptzGoUpStop()
    {
        ptzGo(Code.UP);
        delayStop();
    }

    //云台向下然后停止
    public void ptzGoDownStop()
    {
        ptzGo(Code.DOWN);
        delayStop();
    }

    //云台向左然后停止
    public void ptzGoLeftStop()
    {
        ptzGo(Code.LEFT);
        delayStop();
    }

    //云台向右然后停止
    public void ptzGoRightStop()
    {
        ptzGo(Code.RIGHT);
        delayStop();
    }

    //拉远 需设备支持
    public void ptzGoFar()
    {
        ptzGo(Code.FAR);
    }

    //拉近 需设备支持
    public void ptzGoNear()
    {
        ptzGo(Code.NEAR);
    }

    //云台停止
    public void ptzGoStop()
    {
        ptzGo(Code.STOP);
    }

    //开始监听
    public void startAudio()
    {
        if (isMonitoring() && ismCanControl() && mIsAudio == false)
        {
            mIsAudio = true;
            mMediaFetch.startAudio();
        }
    }

    //结束监听
    public void endAudio()
    {
        if (isMonitoring() && ismCanControl() && mIsAudio == true)
        {
            mIsAudio = false;
            mMediaFetch.endAudio();
        }
    }

    //监听开关切换
    public boolean switchAudio()
    {
        if (mIsAudio)
        {
            endAudio();
        }
        else
        {
            startAudio();
        }
        return mIsAudio;
    }

    //开始对讲
    public void startTalk()
    {
        if (isMonitoring() && ismCanControl() && mIsTalk == false)
        {
            mIsTalk = true;
            mMediaFetch.startTalk();
        }
    }

    //开始对讲
    public void startTalk(int enctype, byte[] eid, byte[] key, byte[] iv)
    {
        if (isMonitoring() && ismCanControl() && mIsTalk == false)
        {
            mIsTalk = true;
            mMediaFetch.startTalk(enctype, eid, key, iv);
        }
    }

    //停止对讲
    public void endTalk()
    {
        if (isMonitoring() && ismCanControl() && mIsTalk == true)
        {
            mIsTalk = false;
            mMediaFetch.endTalk();
        }
    }

    //对讲开关切换
    public boolean switchTalk()
    {
        if (mIsTalk)
        {
            endTalk();
        }
        else
        {
            startTalk();
        }
        return mIsTalk;
    }

    public Integer getSumSize()
    {
        if (isMonitoring())
        {
            return mMediaFetch.getSumSize();
        }
        return 0;
    }

    private DelayStop mDelayStop;

    private class DelayStop implements Runnable
    {
        public void run()
        {
            ptzGoStop();
        }
    }

    private void delayStop()
    {
        if (mDelayStop != null)
            mWorkHandler.removeCallbacks(mDelayStop);
        mDelayStop = new DelayStop();
        mWorkHandler.postDelayed(mDelayStop, 1000);
    }

    private boolean mIsAudio;
    private boolean mIsTalk;
    private boolean mIsRecording;

    public boolean ismIsRecording()
    {
        return mIsRecording;
    }

    public boolean ismIsAudio()
    {
        Log.d(TAG, "audio" + String.valueOf(mIsAudio));
        return mIsAudio;
    }

    public boolean ismIsTalk()
    {
        Log.d(TAG, "talk" + String.valueOf(mIsTalk));
        return mIsTalk;
    }

    public void startMonitor(int mChannelID)
    {
        mMode = Mode.MONITOR;
        mMediaFetch.StartRealPlay(mChannelID);
        Log.i(TAG, "startMonitor " + mChannelID);
    }

    public void startMonitor(int enctype, byte[] eid, byte[] key, byte[] iv, int mChannelID)
    {
        mMode = Mode.MONITOR;
        mMediaFetch.StartRealPlay(enctype, eid, key, iv, mChannelID);
        Log.i(TAG, "startMonitor " + mChannelID);
    }

    public void stopMonitor()
    {
        if(mMode == Mode.MONITOR)
        {
            mMode = Mode.NONE;
            mMediaFetch.StopRealPlay();
            Log.i(TAG, "stopMonitor");
        }
    }

    public void startVodPlay(String path)
    {
        mMode = Mode.PLAY;
        mMediaFetch.VODStartPlayRecord(path);
    }

    public void stopVodPlay()
    {
        if(mMode == Mode.PLAY)
        {
            mMode = Mode.NONE;
            mMediaFetch.VODStopPlayRecord();
        }
    }

    //开始录像
    public void startRecord()
    {
        if (isMonitoring() && ismCanControl() && mIsRecording == false)
        {
            Date now = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss",
                    Locale.CHINA);
            String desFile = FileUtils.VIDEOPATH + "/"
                    + mCameraInfo.getCamerain() + "_" + format.format(now);
            mIsRecording = true;
            mMediaFetch.startRecord(desFile + FileUtils.VIDEOEXT);
//            mMediaFetch.snap(desFile + FileUtils.SNAPEXT);
        }
    }

    //开始录像,指定录像文件路径
    public void startRecord(String filePath)
    {
        if (isMonitoring() && ismCanControl() && mIsRecording == false)
        {
            mIsRecording = true;
            mMediaFetch.startRecord(filePath);
        }
    }

    //停止录像
    public void stopRecord()
    {
        if (isMonitoring() && ismCanControl() && mIsRecording == true)
        {
            mIsRecording = false;
            mMediaFetch.stopRecord();
        }
    }

    //抓图
    public void snap()
    {
        if (isMonitoring() && ismCanControl())
        {
            String desFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "snap.jpg";
            FileUtils.deleteFile(desFile);
            mMediaFetch.snap(desFile);
        }
    }

    //抓图 指定路径
    public void snap(String filePath)
    {
        if (isMonitoring() && ismCanControl())
        {
            mMediaFetch.snap(filePath);
        }
    }
}
