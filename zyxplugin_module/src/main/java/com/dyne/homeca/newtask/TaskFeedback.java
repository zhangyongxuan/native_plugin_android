package com.dyne.homeca.newtask;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.dyne.homeca.module.Feedback;

public abstract class TaskFeedback implements Feedback {
	private static TaskFeedback _instance = null;

	public static enum Type {
		DIALOG_MODE, REFRESH_MODE, PROGRESS_MODE
	};

	public static TaskFeedback getInstance(Type type, Context context) {
		switch (type) {
		case DIALOG_MODE:
			_instance = DialogFeedback.getInstance();
			break;
		case REFRESH_MODE:
			_instance = RefreshAnimationFeedback.getInstance();
			break;
		case PROGRESS_MODE:
			_instance = ProgressBarFeedback.getInstance();
		}
		_instance.setContext(context);
		return _instance;
	}

	protected Context _context;

	protected void setContext(Context context) {
		_context = context;
	}

	public Context getContent() {
		return _context;
	}

	public boolean isAvailable() {
		return _instance != null;
	}

	@Override
	public void start(CharSequence text) {
	}

	@Override
	public void cancel(CharSequence text) {
	}

	@Override
	public void success(CharSequence text) {
	}

	@Override
	public void failed(CharSequence text) {
	}

	@Override
	public void update(Object arg0) {
	}

	@Override
	public void setIndeterminate(boolean indeterminate) {
	}

	@Override
	public void success() {
		success("");
	}
}

/**
 *
 */
class DialogFeedback extends TaskFeedback {
	private static DialogFeedback _instance = null;

	public static DialogFeedback getInstance() {
		if (_instance == null) {
			_instance = new DialogFeedback();
		}
		return _instance;
	}

	private ProgressDialog _dialog = null;

	@Override
	public void cancel(CharSequence text) {
		success(text);
	}

	@Override
	public void success(CharSequence text) {
		if (_dialog != null) {
			_dialog.dismiss();
		}

		if (!TextUtils.isEmpty(text))
			Toast.makeText(_context, text, Toast.LENGTH_LONG).show();
	}

	@Override
	public void failed(CharSequence text) {
		success(text);
	}

	@Override
	public void start(CharSequence text) {
		_dialog = ProgressDialog.show(_context, "", text, true);
		_dialog.setCancelable(true);
	}

	@Override
	public void setIndeterminate(boolean indeterminate) {
		// TODO Auto-generated method stub

	}
}

/**
 *
 */
class RefreshAnimationFeedback extends TaskFeedback {
	private static RefreshAnimationFeedback _instance = null;

	public static RefreshAnimationFeedback getInstance() {
		if (_instance == null) {
			_instance = new RefreshAnimationFeedback();
		}
		return _instance;
	}

}

/**
 *
 */
class ProgressBarFeedback extends TaskFeedback {

	private static ProgressBarFeedback _instance = null;

	public static ProgressBarFeedback getInstance() {
		if (_instance == null) {
			_instance = new ProgressBarFeedback();
		}
		return _instance;
	}

}
